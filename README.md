# Local Php Security Checker
Build of CLI too for [local-php-security-checker](https://github.com/fabpot/local-php-security-checker)

## Run
```bash
docker run --rm -v `pwd`:/opt/php/ oxcom/php-security-checker:alpine --
```

## In the image
- GIT
- OpenSSH
- local-php-security-checker

## Local build
```bash
docker build . --rm -t php-security:latest -f ./debian/Dockerfile
docker run --rm -it php-security:latest
```

## GitLab CI usage
```yaml
test:docker:
    services:
        # Run docker in docker with MTU 1450 (hack for open stack)
        - name: docker:dind
          command: [ "--mtu=1450" ]
    image: docker:latest
    script:
        - docker run --rm -v `pwd`:/opt/php oxcom/php-security-checker:debian --
```
